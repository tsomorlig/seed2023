import os
import subprocess
from flask import Flask
app = Flask(__name__)

def run_command(command):
    return subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()

@app.route("/")
def main():
#    return run_command("echo my host ip is: $(nslookup host.docker.internal | grep Server | awk {'print $2'})")
    return run_command("echo My $(nslookup host.docker.internal | grep Server)")

@app.route('/how are you')
def hello():
    return 'I am good, how about you?'

if __name__ == "__main__":
    app.run()
