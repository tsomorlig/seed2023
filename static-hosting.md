1. Create bucket us-east-1 N.Virginia
2. Uncheck Block all public access. Check I acknowledge
3. Click create bucket
4. Bucket > Properties > Static Enable
Hosting type: Host a static website
Index document: index.html
Error document: error.html
5. Object > Add files index & error
Object > Add folder img 
Click Upload
6. Bucket > Permissions > Bucket Policy 
Paste bucket_policy.json & edit Resource
